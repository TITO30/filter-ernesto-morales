import { LitElement } from "lit-element";

const ApiUrl = "http://localhost:8000/posts.json";

export default class GetMyPostsDm extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  _handleSuccessResponse(data) {
    try {
      let users = this._formatData(data);
      this.dispatchEvent(new CustomEvent("success-call", { detail: users }));
    } catch (e) {
      console.log(e);
      this._handleErrorResponse(data);
    }
  }

  _formatData(users) {
    let formatPosts = [];
    users.forEach((user) => {
      formatPosts.push({
        name: user.name,
        age: user.age,
      });
    });
    return formatPosts;
  }

  _handleErrorResponse(dataErr) {
    let erroConfig = {
      title: "Error de conexion",
      body: "Intentan de nuevio mas tarde",
    };
    this.dispatchEvent(new CustomEvent("error-call", { detail: erroConfig }));
  }

  async generateRequest() {
    await fetch(ApiUrl)
      .then((response) => response.json())
      .then((response) => {
        this._handleSuccessResponse(response);
      })
      .catch((err) => {
        console.log("este es el json", err);
        this._handleErrorResponse(err);
      });
  }
}

customElements.define("get-my-posts", GetMyPostsDm);
